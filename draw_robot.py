# Code to draw the FOLL-E interface.
#
# Author: Simon Vandevelde <s.vandevelde@kuleuven.be>

import pygame as pg
import os
from idp_engine.API import KB
from aruco_detect import interpret_image
import cv2 
import time
import numpy as np
import datetime
from idp_engine.utils import IDPZ3Error 

# SCREEN_WIDTH = 1700
SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 700
MODE = "discrim"
LOG_FP = open(str(datetime.datetime.now()).replace(":",";") + '.txt', 'w')

# Open cv2 window first, to ensure focus starts on the robot screen.
cv2.namedWindow('detected')
cv2.moveWindow('detected', SCREEN_WIDTH, 0)
cv2.waitKey(1)

def current_time():
    return str(datetime.datetime.now().time())


def log(string):
    LOG_FP.write(f'{current_time()} -- {string}\n')

class Component(pg.sprite.Sprite):

    images = {'arm': {'white': './Images/arm.png',
                      'red': './Images/arm_red.png',
                      'green': './Images/arm_green.png',
                      'blue': './Images/arm_blue.png'},
              'leg': {'white': './Images/leg_shoe.png',
                      'red': './Images/leg_shoe_red.png',
                      'green': './Images/leg_shoe_green.png',
                      'blue': './Images/leg_shoe_blue.png'},
              'body': {'white': './Images/body.png',
                       'red': './Images/body_red.png',
                       'green': './Images/body_green.png',
                       'blue': './Images/body_blue.png'},
              'head': {'white': './Images/head.png',
                       'red': './Images/head_red.png',
                       'green': './Images/head_green.png',
                       'blue': './Images/head_blue.png'},
              'helmet': {'white': './Images/helmet.png'}}
    pos = {'arm': {True: (-222, 155),
                   False: (-15, 155)},
           'leg': {True: (-200, 380),
                   False: (-45, 380)},
           'body': {True: (-120, 195)},
           'head': {True: (-120, 0)},
           'helmet': {True: (-120, -30)}}

    offset = {1: (120, 30), 2: (120, 260), 3: (120, 490),
              4: (950, 30), 5: (950, 260), 6: (950, 490)}

    def __init__(self, comp_type, left=True, color='white',
                 offset_id=0):
        super().__init__()

        self.type = comp_type
        self.left = left
        self.color = color
        self.offset_id = offset_id

        assert self.type in self.images.keys(), 'unknown component'

        if offset_id == 0:
            offset_x = 595
            offset_y = 155
            self.scale = 1
        elif offset_id in [1, 2, 3, 4, 5, 6]:
            offset_x, offset_y = self.offset[offset_id]
            self.scale = 0.4
            # new_width = round(self.image.get_width()*scale)
            # new_height = round(self.image.get_height()*scale)
            #self.image = pg.transform.scale(self.image,
            #                                (new_width, new_height),
            #                                scale)
        self.image = self.load_image(self.type, self.left, self.color, self.scale)

        self.rect = self.image.get_rect()
        x, y = self.pos[self.type][self.left]
        self.rect.center = [x*self.scale + offset_x, y*self.scale + offset_y]

    def load_image(self, comp_type, left, color, scale=0):
        image = pg.image.load(self.images[comp_type][color])
        if scale:
            new_width = round(image.get_width()*scale)
            new_height = round(image.get_height()*scale)
            image = pg.transform.scale(image, (new_width, new_height))
        if self.left:
            return pg.transform.flip(image, True, False)
        else:
            return image

    def update(self):
        self.image = self.load_image(self.type,
                                     self.left, self.color, self.scale)

    def paint(self, color):
        self.color = color

class Mark(pg.sprite.Sprite):

    offset = {1: (230, 120), 2: (230, 350), 3: (230, 580),
              4: (720, 120), 5: (720, 350), 6: (720, 580),
              7: (475, 300)}

    def __init__(self, check=False, invert=False, offset_id=0):
        super().__init__()

        self.check = check
        self.invert = invert
        self.image = self.load_image()

        x, y = self.offset[offset_id]

        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def load_image(self):
        if self.check:
            if self.invert:
                image = pg.image.load('./Images/check_red.png')
            else:
                image = pg.image.load('./Images/check.png')
            scale = 0.2
        else:
            if self.invert:
                image = pg.image.load('./Images/cross_green.png')
            else:
                image = pg.image.load('./Images/cross.png')
            scale = 0.3
        new_width = round(image.get_width()*scale)
        new_height = round(image.get_height()*scale)
        image = pg.transform.scale(image, (new_width, new_height))
        return image

    def set_check(self):
        if self.check == False:
            self.check = True
            self.image = self.load_image()

    def set_cross(self):
        if self.check == True:
            self.check = False
            self.image = self.load_image()

class Loading(pg.sprite.Sprite):

    def __init__(self):
        super().__init__()

        self.image = self.load_image()

        self.rect = self.image.get_rect()
        self.rect.center = [300, 100]

    def load_image(self):
        image = pg.image.load('./Images/loading.png')
        # scale = 1.0
        # new_width = round(image.get_width()*scale)
        # new_height = round(image.get_height()*scale)
        # image = pg.transform.scale(image, (new_width, new_height))
        return image



class Robot():

    def __init__(self, robot_id=0):
        self.robot_id = robot_id
        self.l_arm = Component('arm', left=True, offset_id=robot_id)
        self.r_arm = Component('arm', left=False, offset_id=robot_id)
        self.body = Component('body', offset_id=robot_id)
        self.head = Component('head', offset_id=robot_id)
        self.l_leg = Component('leg',  left=True, offset_id=robot_id)
        self.r_leg = Component('leg',  left=False, offset_id=robot_id)
        self.components = {'left_arm': self.l_arm, 'right_arm': self.r_arm,
                           'body': self.body, 'head': self.head,
                           'left_leg': self.l_leg, 'right_leg': self.r_leg,
                           }

    def apply_model(self, model):
        """ Apply a generated IDP model to the robot. """

        # Remove the helmet if it's not needed.
        if 'helmet' in model and model['helmet']:
            try:
                self.helmet = Component('helmet', offset_id=self.robot_id)
                self.components['helmet'] = self.helmet
            except:
                pass
        else:
            try:
                del self.helmet
                del self.components['helmet']
            except:
                pass

        for comp, kleur in model['Color'].items():
            self.components[comp].paint(kleur)

        for comp in self.components.values():
            comp.update()



    def apply_prop(self, pos_neg_models):
        """ Apply a propagation dictto the robot. """
        pos_model, neg_model = pos_neg_models

        for comp in self.components.keys():
            if comp in pos_model['Color']:
                self.components[comp].paint(pos_model['Color'][comp])
            else:
                self.components[comp].paint('white')

        self.helmet = Component('helmet', offset_id=self.robot_id)
        if len(pos_model['helmet']) > 0:
            # Add helmet.
            try:
                self.has_helmet = True
                self.components['helmet'] = self.helmet
            except:
                pass
        else:
            # Remove helmet.
            try:
                self.has_helmet = False
                del self.components['helmet']
            except:
                pass

        for comp in self.components.values():
            comp.update()


def run_IDP(method, extra_constraint, model=False):
    idp = KB()
    idp.type('component', ['head', 'body', 'left_arm', 'right_arm',
                           'left_leg', 'right_leg'])
    idp.type('color', ['red', 'blue', 'green'])

    idp.predicate('IsArm', ['component'], [('left_arm'), ('right_arm')])
    idp.predicate('IsLeg', ['component'], [('left_leg'), ('right_leg')])
    idp.proposition('helmet')
    if not model:
        # Model expansion.
        idp.function('Color', ['component'], 'color')
    else:
        # Model check.
        idp.function('Color', ['component'], 'color',
                     model['Color'])
        # If helmet, set its value accordingly. (This is done using a constraint due to a bug in IDP-Z3Py)
        if model['helmet']:
            idp.constraint('helmet().')
        else:
            idp.constraint('~helmet().')

    idp.constraint(extra_constraint)

    if method == 'mx':
        return idp.model_expand(1)[0]
    elif method == 'prop':
        return idp.propagate()

def next_level(group, level, models, minibots, marks, errormark, robot0, screen=None):
    # Sleep to show the solution of previous level if it is solved
    if screen is not None and level == 0:
        time.sleep(4)
    elif screen is not None:
        time.sleep(1)
    # First, remove all components.
    for sprite in group.sprites():
        if hasattr(sprite, 'type'):
            group.remove(sprite)
    
    # Remove marks
    for mark in marks:
        group.remove(mark)
        group.remove(errormark)

    # Change the level, update the models, remove the marks.
    level += 1
    if level >= len(models[0]):
        if screen is not None:
            finished_animation(minibots, group, screen, robot0)
        level = 0

    for model, robot in zip(models, minibots):
        robot.apply_model(model[level])
    
    # Clear the propagate robot.
    robot_white = {'Color': {'head': 'white', 'body': 'white', 'left_arm': 'white', 'right_arm': 'white', 'left_leg': 'white', 'right_leg': 'white'}, 'helmet': False}
    robot0.apply_model(robot_white)

    # Add all the components again.
    for robot in [robot0] + minibots:
        for component in robot.components.values():
            group.add(component)

    log(f'Started level {level+1}')
    return group, level 
    
def finished_animation(minibots, group, screen, robot0):
    robot_red = {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False}
    robot_green = {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False}
    robot_blue = {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False}
    
    models = [robot_red, robot_green, robot_blue]
    model0 = [ {'Color': {'head': 'red', 'body': 'blue', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': False},
              {'Color': {'head': 'green', 'body': 'red', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'green', 'right_leg': 'red'}, 'helmet': False},
              {'Color': {'head': 'blue', 'body': 'green', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'green'}, 'helmet': False},
              ]
    
    for i in range(10):
        # First, remove all components.
        for sprite in group.sprites():
            if hasattr(sprite, 'type'):
                group.remove(sprite)

        for j in range(len(minibots)):
            minibots[j].apply_model(models[(j + i)%3])
        
        robot0.apply_model(model0[i%3])

        # Add all the components again.
        for robot in [robot0] + minibots:
            for component in robot.components.values():
                group.add(component)

        #draw the robots
        group.draw(screen)
        if MODE == "discrim":
            # Draw the lines.
            pg.draw.line(screen, (0, 0, 0), (270, 0), (270, SCREEN_HEIGHT))
            pg.draw.line(screen, (0, 0, 0), (680, 0), (680, SCREEN_HEIGHT))
        pg.display.flip()
        time.sleep(0.6)

def main():
    log('START')

    level = 0

    robot0 = Robot(0)
    robot1 = Robot(1)
    robot2 = Robot(2)
    robot3 = Robot(3)
    robot4 = Robot(4)
    robot5 = Robot(5)
    robot6 = Robot(6)

    minibots = [robot1, robot2, robot3, robot4, robot5, robot6]


    # Todo: generate these in robot
    mark1 = Mark(check=False, offset_id=1)
    mark2 = Mark(check=False, offset_id=2)
    mark3 = Mark(check=False, offset_id=3)
    mark4 = Mark(check=True, invert=True, offset_id=4)
    mark5 = Mark(check=True, invert=True, offset_id=5)
    mark6 = Mark(check=True, invert=True, offset_id=6)
    errormark = Mark(check=False, offset_id=7)

    marks = [mark1, mark2, mark3, mark4, mark5, mark6]

    model1 = [
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'blue', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'green', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'green', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'green', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'green', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            ]

    model2 = [
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'blue', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'red', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'red', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'red', 'left_arm': 'blue', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            ]

    model3 = [
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'blue', 'left_arm': 'red', 'right_arm': 'green', 'left_leg': 'blue', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'blue', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            ]

    model4 = [
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'blue', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'red', 'left_arm': 'green', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'red', 'left_arm': 'green', 'right_arm': 'red', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            ]

    model5 = [
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'red', 'right_leg': 'red'}, 'helmet': False},
            ]

    model6 = [
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'blue', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'green', 'right_leg': 'red'}, 'helmet': False},
            {'Color': {'head': 'red', 'body': 'red', 'left_arm': 'red', 'right_arm': 'red', 'left_leg': 'green', 'right_leg': 'red'}, 'helmet': True},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'blue', 'body': 'blue', 'left_arm': 'blue', 'right_arm': 'blue', 'left_leg': 'blue', 'right_leg': 'blue'}, 'helmet': True},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'blue', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'green'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'green', 'right_leg': 'blue'}, 'helmet': False},
            {'Color': {'head': 'green', 'body': 'green', 'left_arm': 'green', 'right_arm': 'green', 'left_leg': 'blue', 'right_leg': 'green'}, 'helmet': False},
            ]

    models = [model1, model2, model3, model4, model5, model6]



    # Init screen, add all relevant sprites (components, marks, loading)
    os.environ['SDL_VIDEO_WINDOW_POS'] = "0,0"  # Window in top-left corner
    pg.init()
    screen = pg.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
    group = pg.sprite.RenderPlain()

    # Set the pygame icon.
    icon = pg.image.load('./Images/head_red.png')
    pg.display.set_icon(icon)
    pg.display.set_caption('FOLL-E')

    # Set up robots with correct model.
    for model, robot in zip(models, minibots):
        robot.apply_model(model[level])

    for robot in [robot0] + minibots:
        for component in robot.components.values():
            group.add(component)

    loading = Loading()

    running = True
    old_formulae = []
    old_time = time.time()

    cam = cv2.VideoCapture(0)

    new_level = True
    nb_correct = 0

    while running:
        screen.fill((255, 255, 255))
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key in [pg.K_ESCAPE, pg.K_q]:
                    running = False
                    import sys
                    sys.exit(0)
                elif event.key in [pg.K_l]:
                    group, level = next_level(group, level, models, minibots, marks, errormark, robot0)
                    old_formulae = []

        if nb_correct == 6:
            nb_correct = 0
            group, level = next_level(group, level, models, minibots, marks, errormark, robot0, screen)
            
        # Interpret the camera input.
        cv2.waitKey(1)
        old_time = time.time()
        ret_val, img = cam.read()
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        formulae = interpret_image(img)
        # formulae = ['']

        # Run IDP if a new formula was found.
        if formulae and formulae[0] and formulae != old_formulae:
            # Clear the propagate robot and marks.
            robot_white = {'Color': {'head': 'white', 'body': 'white', 'left_arm': 'white', 'right_arm': 'white', 'left_leg': 'white', 'right_leg': 'white'}, 'helmet': False}
            robot0.apply_model(robot_white)
            for mark in marks:
                group.remove(mark)
                group.remove(errormark)

            nb_correct = 0
            log(f'Tried "{formulae}"')
            # Add loading bar, redraw screen.
            group.add(loading)
            group.draw(screen)
            pg.display.flip()

            # If the marks have not been added, add them.
            if new_level:
                # for mark in marks:
                #     group.add(mark)
                new_level = False

            old_formulae = formulae
            # model = run_IDP('mx', '.'.join(formulae))
            # robot0.apply_model(model)
            try:
                robot0.apply_prop(run_IDP('prop', '.'.join(formulae)))
            except IDPZ3Error:
                # If IDP-Z3 fails because the sentence was incorrect (such as ColorOf('red') = 'red'),
                # show an error sign.
                group.remove(loading)
                group.add(errormark)
                continue
            except IndexError:
                # When no solutions were found (e.g., 'isArm(leg)').
                group.remove(loading)
                group.add(errormark)
                continue

            # Add or remove helmet.
            if hasattr(robot0, 'has_helmet') and robot0.has_helmet:
                group.add(robot0.helmet)
            else:
                # Delete the helmet.
                for sprite in group.sprites():
                    if hasattr(sprite, 'type') and sprite.type == 'helmet' and sprite.offset_id == 0:
                        group.remove(sprite)

            # Test if models 1-3 are feasible and models 3-6 are infeasible.
            for i in range(0, 6):
                a = time.time()
                try:
                    run_IDP('mx', '.'.join(formulae), models[i][level])
                    # Rule includes correct robots.
                    if 0 <= i <= 2:
                        group.add(marks[i])
                        marks[i].set_check()
                        nb_correct += 1
                    else:
                        group.remove(marks[i])
                except IndexError:
                    # Rule excludes incorrect robots.
                    if 3 <= i <= 5:
                        group.add(marks[i])
                        marks[i].set_cross()
                        nb_correct += 1
                    else:
                        group.remove(marks[i])

                # Draw the marks for the robots.
                group.draw(screen)
                pg.display.flip()


            group.remove(errormark)
            group.remove(loading)


        # Draw the robot components.
        group.draw(screen)

        if MODE == "discrim":
            # Draw the lines.
            pg.draw.line(screen, (0, 0, 0), (270, 0), (270, SCREEN_HEIGHT))
            pg.draw.line(screen, (0, 0, 0), (680, 0), (680, SCREEN_HEIGHT))

        pg.display.flip()
    pg.quit()


main()

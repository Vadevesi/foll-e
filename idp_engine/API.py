"""
The IDP-Z3 API allows fluent interaction with the IDP system from inside
Python. Its goal is to allow the creation of knowledge bases on-the-fly,
in a Pythonic way.
"""
import re
from typing import Type, List, Dict  # Typing deprecated in p3.9
from idp_engine import IDP
from contextlib import redirect_stdout


class KB():
    """ Class representing a knowledge base.

    In short, such a KB is represented by three components:
        * The symbols (which go in the voc / structure)
        * The constraints (which go in the theory)
        * The definitions (which go in the theory)

    There are multiple execution methods supported:
        * model expansion
        * model optimization (min/max)
        * propagation

    When such an execution method is called, the following few steps are taken:
        1. The Pythonic KB is converted into an FO(.) KB (in string format)
        2. This is fed to the idp_engine, which performs the execution
        3. The output of the idp_engine is parsed and converted back into
            Pythonic form

    Args:
        None

    Attributes:
        symbols (`list` of `IDPSymbolObject`): a list containing the KB's
            symbols.
        constraints (`list` of `IDPConstraint`): list of KB's constraints.
        definitions (`list` of `IDPDefinition`): list of KB`s definitions.
    """
    def __init__(self):
        """ Inititalize symbol dict and definition/constraint lists.
        """
        self.symbols: Dict[str, IDPSymbolObject] = {}
        self.constraints: List[IDPConstraint] = []
        self.definitions: List[IDPDefinition] = []

        self.symbols['Bool'] = IDPStandardType('Bool')
        self.symbols['Int'] = IDPStandardType('Int')
        self.symbols['Real'] = IDPStandardType('Real')

    def type(self, name, enumeration):
        """ Add new type to the KB.

        Args:
            name (str)
            enumeration (`list` of str/int): the type's domain of values.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name.
        """
        if name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {name}')
        self.symbols[name] = IDPType(name, enumeration)

    def predicate(self, symbol_name, arguments, enumeration=[]):
        """ Add new predicate to the KB.

        A predicate is defined by its name, its arguments, and (optionally) an
        enumeration. The arguments of the predicate *must* be known types,
        i.e., types that have previously been created.

        Args:
            symbol_name (str)
            arguments (`list` of str): The types of the predicate.
            enumeration (`list` of str or int, optional): Optional enumeration
            for the predicate.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the arguments contains an unknown type.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.verify_types(arguments)

        self.symbols[symbol_name] = IDPPredicate(symbol_name, arguments,
                                                 enumeration)

    def function(self, symbol_name: str, arguments: List, returntype: str,
                 enumeration: Dict = {}, default_val=None):
        """ Add new function to the KB.

        A function is defined by its name, its arguments, a return type,
        (optionally) an enumeration, and (optionally) a default enumeration
        value. Both the arguments and the return type should be valid types.

        Note:
            If an enumeration is used, it **must** either be complete, or a
            default value should be given.
        Args:
            symbol_name (str)
            arguments (`list` of str): The types of the function.
            returntype (str): The type of the return value.
            enumeration (`list` of str or int, optional): Optional enumeration
                for the function.
            default_val (str or int, optional): Optional default value for the
                enumeration.

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the arguments contains an unknown type.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.verify_types(arguments + [returntype])

        self.symbols[symbol_name] = IDPFunction(symbol_name, arguments,
                                                returntype, enumeration,
                                                default_val)

    def proposition(self, symbol_name, value: bool = False):
        """ Add new proposition to the KB.

        A proposition is defined by its name, and an optional boolean value.

        Args:
            symbol_name (str)
            value (bool, optional)

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.symbols[symbol_name] = IDPProposition(symbol_name, value)

    def boolean(self, symbol_name, value: bool = False):
        """ Add new boolean to the KB.

        A proposition is defined by its name, and an optional boolean value.

        Note:
            This method is a clone of the proposition.

        Args:
            symbol_name (str)
            value (bool, optional)

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.boolean(symbol_name, value)

    def constant(self, symbol_name: str, returntype: str,
                 value=None):
        """ Add new constant the KB.

        A constant is defined by its name, its returntype, and (optionally) its
        value. The return type of the constant should already exist.

        Args:
            symbol_name (str)
            returntype (str)
            value (str or int, optional)

        Returns:
            None

        Raises:
            ValueError: If a symbol already exists with the same name
                or if the return type is unknown.
        """
        if symbol_name in self.symbols:
            raise ValueError(f'Duplicate symbol name: {symbol_name}')
        self.verify_types([returntype])

        self.symbols[symbol_name] = IDPConstant(symbol_name, returntype, value)

    def verify_types(self, arguments):
        """ Verifies whether types are already known to the KB

        Args:
            arguments (`list` of str): the types to verify.

        """
        for arg in arguments:
            if arg not in self.symbols.keys():
                raise ValueError(f'Type {arg} does not exist')

    def constraint(self, constraint: str):
        """ Add a constraint to the KB

        Note:
            This constraint should already be in FO(.) format.

        Args:
            constraint (str): the constraint in FO(.) format.
        """

        self.constraints.append(IDPConstraint(constraint))

    def definition(self, rules: [str]):
        """ Add a definition to the KB

        Note:
            The definition rules should already be in FO(.) format.

        Args:
            rules (`list` of str): the rules in FO(.) format.
        """
        self.constraints.append(IDPDefinition(rules))

    def generate_voc(self):
        """ Generates a vocabulary based on the info in the KB.

        Iterates over all symbols (types, predicates, functions, propsitions
        and constants) and formats them for the vocabulary.

        Args:
            None

        Returns:
            str: the vocabulary
        """
        voc = "vocabulary { \n"
        for symbol in self.symbols.values():
            voc += symbol.to_voc() + "\n"
        voc += "}\n\n"
        return voc

    def generate_theory(self):
        """ Generates a theory based on the info in the KB.

        Iterates over all constraints and definitions, and formats them in a
        theory.

        Args:
            None

        Returns:
            str: the theory
        """
        theo = "theory { \n"
        for constraint in self.constraints:
            theo += constraint.to_theory() + "\n"
        for definition in self.definitions:
            theo += definition.to_theory() + "\n"
        theo += "}\n\n"
        return theo

    def generate_structure(self):
        """ Generates a structure based on the info in the KB.

        Iterates over all symbols (types, predicates, functions, propsitions
        and constants) and formats them for the vocabulary.

        Args:
            None

        Returns:
            str: the structure
        """
        struct = "structure { \n"
        for symbol in self.symbols.values():
            struct += symbol.to_structure() + "\n"
        struct += "}\n\n"
        return struct

    def generate_KB(self, inf):
        """ Generate a full fledged IDP knowledge base.

        Note:
            The content of the `main` block should be supplied.

        Args:
            inf (str): the contents of the KB's `main`, in IDP format.
        """
        KB = self.generate_voc() + self.generate_structure() \
            + self.generate_theory() \
            + f"procedure main() {{\n\t{inf} \n}}"
        return KB

    def model_expand(self, nb_models=10):
        """ Perform model expansion on the KB.

        Args:
            nb_models (int, optional): the max number of models to generate.

        Returns:
            `list` of `dict`: all generated models
        """
        KB = self.generate_KB(f'pretty_print(model_expand(T, S, {nb_models}))')
        output = self.execute_KB(KB)
        return self.interpret_model(output)

    def optimize(self, term, minimize=True):
        """ Perform optimization (max or min) on the KB.

        Args:
            term (str): the optimization term
            minimize (bool, optional)

        Returns:
            `list` of `dict`: all generated models
        """

        KB = self.generate_KB(f'pretty_print(Problem(T, S)'
                              f'.optimize("{term}", {minimize}))')
        output = self.execute_KB(KB)
        return self.interpret_model(output)

    def minimize(self, term):
        """ Perform minimization on the KB.

        Args:
            term (str): the minimzation term

        Returns:
            `list` of `dict`: all generated models
        """
        return self.optimize(term, minimize=True)

    def maximize(self, term):
        """ Perform maximization on the KB.

        Args:
            term (str): the maximization term

        Returns:
            `list` of `dict`: all generated models
        """
        return self.optimize(term, minimize=False)

    def propagate(self):
        """ Perform propagation on the KB.

        Args:
            None

        Returns:
            `tuple` of `dict`: the postitive facts, and the negative facts
        """
        KB = self.generate_KB('pretty_print(model_propagate(T, S))')
        output = self.execute_KB(KB)
        return self.interpret_prop(output)

    def custom_main(self, main):
        """ Execute a custom main.

        Note:
            The output of the IDP system is returned directly -- parsing it is
            up to the user of this function.

        Args:
            main (str)

        Returns:
            `str`: IDP's output
        """
        KB = self.generate_KB(main)
        return self.execute_KB(KB)

    def execute_KB(self, KB):
        """ Executes an IDP KB in string format.

        Args:
            KB (str): the KB in string format.

        Returns:
            `str`: IDP's output.
        """
        idp = IDP.from_str(KB)
        # Execute IDP, capture the output.
        with open('/tmp/idp_temp.txt', mode='w', encoding='utf-8') \
                as buf, redirect_stdout(buf):
            idp.execute()
        with open('/tmp/idp_temp.txt', mode='r', encoding='utf-8') as fp:
            output = fp.read()
        return output

    def interpret_model(self, output):
        """ Interprets models in IDP's output.

        Mainly to be used after model expansion or optimization.

        Args:
            output (str): IDP's output to parse.

        Returns:
            `list` of `dict`: a list containing all models (as dict)

        """
        # Here be regex.
        models = []
        if output.startswith('No models.'):
            return []
        if 'Model' in output:
            outp_models = re.findall(r'Model \d+\n==========\n(.*?)\n\n',
                                     output, re.DOTALL)
        else:
            outp_models = [output]
        for outp_model in outp_models:
            assignments = outp_model.split('\n')
            model = {}
            for assignment in assignments:
                if assignment == '':
                    continue
                symbol_name, values = assignment.split(' := ')
                values = values.strip('{}')
                symbol_type = self.symbols[symbol_name].symbol_type
                symbol_arity = self.symbols[symbol_name].arity


                # Based on the symbol's type, we interpret the values
                # differently
                if symbol_type == 'proposition':
                    if values:
                        model[symbol_name] = True
                    else:
                        model[symbol_name] = False
                elif symbol_type == 'constant':
                    model[symbol_name] = values
                elif symbol_type == 'predicate':
                    # Find all values (e.g., (x, y, z)).
                    regstr = r'\((.*?)\)(,|$)'
                    values = re.findall(regstr, values)

                    out_values = []
                    # Second var is EOL or `.`. No need to use it.
                    for arg, _ in values:
                        if ',' in arg:
                            arg = tuple(arg.split(','))
                        else:
                            arg = arg.strip()
                        out_values.append(arg)
                    model[symbol_name] = out_values
                else:  # Function
                    regstr = r'\(?(.*?)\)?->(.*?)(,|$)'
                    values = re.findall(regstr, values)
                    out_values = {}
                    # Third var is EOL or `.`. No need to use it, hence the _
                    for arg, value, _ in values:
                        if ',' in arg:
                            arg = tuple(arg.split(','))
                        else:
                            arg = arg.strip()
                        out_values[arg] = value
                    model[symbol_name] = out_values
            models.append(model)
        return models

    def interpret_prop(self, output):
        """ Intepret the propagation output of IDP.

        In short, we collect all output in two dictionaries: one containing
        positive assignments, and one containing the negative assignments.

        Args:
            output (str): IDP's propagation output.

        Returns:
            `tuple` of `dict`: The dicts containing the postitive and negative
                assignments.
        """
        pos_assignments = {}
        neg_assignments = {}
        for symbol_name, symbol in self.symbols.items():
            if symbol.symbol_type == 'type':
                continue
            elif symbol.symbol_type in ['predicate', 'proposition']:
                pos_assignments[symbol_name] = []
                neg_assignments[symbol_name] = []
            elif symbol.symbol_type in ['function']:
                pos_assignments[symbol_name] = {}
                neg_assignments[symbol_name] = {}
            else:
                # In case of constant
                pos_assignments[symbol_name] = None
                neg_assignments[symbol_name] = None


        for line in output.split('\n')[:-2]:
            # Last two lines are "No more consequences \n", we skip those.
            neg, symbol_name, args, _, val = re.findall(r'(Not |)(.*?)\((.*?)\)( -> (.*?)$|)', line)[0]
            symbol_type = self.symbols[symbol_name].symbol_type
            if ',' in args:
                arg = tuple(args.split(','))
            else:
                arg = args
            if neg:
                assignments = neg_assignments
            else:
                assignments = pos_assignments

            if symbol_type in ['predicate', 'proposition']:
                assignments[symbol_name].append(arg)
            elif symbol_type in ['function']:
                assignments[symbol_name][arg] = val
            else:
                assignments[symbol_name] = val
        return pos_assignments, neg_assignments


class IDPSymbolObject():
    """ Parent class for all IDP symbols.

    This class contains some attributes and methods shared bt all IDP symbols.

    Attributes:
        symbol_name (str)
    """
    def __init__(self, name):
        """ Initialize the symbol object.

        Args:
            name (str)

        Raises:
            ValueError: if the name is invalid.
        """
        self.check_name_validity(name)
        self.symbol_name = name

    def to_voc(self):
        """ Dummy function """
        return ''

    def to_theory(self):
        """ Dummy function """
        return ''

    def to_structure(self):
        """ Dummy function """
        return ''

    def check_name_validity(self, name):
        """ Checks if a name is valid.

        Valid names can only contain alfanumerical characters + dashes and
        underscores.

        Args:
            name (str)

        Returns:
            None

        Raises:
            ValueError: if the name is invalid.
        """
        if len(re.findall(r'\w+', name)) != 1:
            raise ValueError(f'Invalid name: {name}')
        elif re.match(r'\d', name):
            raise ValueError(f'Invalid name: {name}'
                             f' (cannot start with number)')

    def check_argument_validity(self, args):
        """ Check if the arguments are valid

        Args:
            args (`list` of str)

        Raises:
            ValueError: If args is not a list
        """
        if type(args) is not list:
            raise ValueError(f'Invalid argument(s) "{args}":'
                             f' arguments must be list')
        # TODO: maybe some type checking? I.e. are the arguments valid types?

    def check_enumeration_validity(self, args, enumeration):
        """ Check if the enumeration is valid w.r.t the arity

        Args:
            args (`list` of str): the arguments of the symbol
            enumeration (`list` of `tuple`)

        Raises:
            ValueError: If the enumeration has an incorrect number of
                arguments.
        """
        if self.rtype != 'Bool' and self.arity > 1:
            keys = enumeration.keys()
        elif self.arity > 0:
            keys = enumeration
        else:
            keys = []

        if len(args) > 1:
            # Check if the enumeration is correct w.r.t. the arity
            arity = len(args)
            for key_tuple in keys:
                if type(key_tuple) is not tuple or len(key_tuple) != arity:
                    raise ValueError(f'Incorrect number of args for'
                                     f' {self.symbol_name}: {key_tuple}')
        elif len(args) == 1:
            for key in keys:
                if type(key) not in [int, str, float]:
                    raise ValueError(f'Incorrect number of args for'
                                     f' {self.symbol_name}: {key}')

        # TODO: maybe value checking? I.e. are the values correct, given the
        # type?

        # TODO: verify whether enumeration is complete OR has a default value.


class IDPType(IDPSymbolObject):
    """ Class representing an IDP type.

    IDP types consist of two components: a name, and an enumeration.

    Attributes:
        symbol_name (str)
        enumeration (`list` of str/int)
    """
    def __init__(self, name: str, enumeration):
        """ Initialize the Type.

        Args:
            name (str)
            enumeration (`list` or `range`)

        Raises:
            ValueError: if the enumeration is of incorrect type.

        """
        super().__init__(name)
        self.symbol_type = 'type'

        if type(enumeration) not in [range, list]:
            raise ValueError(f'Enumeration for {name} should be list or range')

        self.enumeration = [str(x) for x in enumeration]

    def to_voc(self):
        """ Convert the Type into its vocabulary format.

        Args:
            None

        Returns:
            str: the Type converted into Vocabulary format.
        """
        if type(self.enumeration) is range:
            return (f"\ttype {self.symbol_name} :="
                    f" {{{str(self.enumeration.start)}"
                    f"..{self.enumeration.stop}}}")
        else:
            return (f"\ttype {self.symbol_name} :="
                    f" {{{', '.join(self.enumeration)}}}")

class IDPStandardType(IDPType):
    """ Class representing a built-in IDP type. These have no enumeration.

    IDP types consist of two components: a name.

    Attributes:
        symbol_name (str)
    """
    def __init__(self, name: str):
        """ Initialize the Type.

        Args:
            name (str)

        """
        super().__init__(name, [])
        self.symbol_type = 'type'

    def to_voc(self):
        """ Built-in types are not added to vocabulary.

        Returns:
            str: empty str
        """
        return ''


class IDPFunction(IDPSymbolObject):
    """ Class representing an IDP function.

    IDP functions consist of five components:
        * a name;
        * its arguments;
        * its return type;
        * (optional) the enumeration;
        * (optional) the default value.

    Attributes:
        symbol_type (str)
        arguments (`list` of str)
        rtype (str): the returntype
        enumeration (`list` of `tuple`)
        arity (int)
        default_val (str)

    """
    def __init__(self, symbol_name: str, arguments: List[str],
                 returntype: str, enumeration: Dict = {}, default_val=None):
        super().__init__(symbol_name)
        self.symbol_type = 'function'

        self.arguments = arguments
        self.rtype = returntype
        self.enumeration = enumeration
        self.arity = len(self.arguments)
        self.default_val = default_val

        self.check_argument_validity(arguments)
        self.check_enumeration_validity(arguments, enumeration)

    def to_voc(self):
        """ Convert the Function into vocabulary format.

        Args:
            None

        Returns:
            str: the Function in vocabulary format.
        """
        return (f'\t{self.symbol_name} : ({" * ".join(self.arguments)})'
                f' -> {self.rtype}')

    def to_structure(self):
        """ Convert the Function's enumeration into structure format.

        Args:
            None

        Returns:
            str: the Function's enumeration in structure format.
        """
        if self.enumeration is None or len(self.enumeration) == 0:
            return ''

        assignments = []
        if self.arity > 1:
            for key_tuple, value in self.enumeration.items():
                key_tuple = [str(x) for x in key_tuple]
                assignments.append(f'({", ".join(key_tuple)}) -> {value}')
        elif self.arity == 1:
            for key, value in self.enumeration.items():
                key = str(key)
                assignments.append(f'({key}) -> {value}')
        struct = f'\t{self.symbol_name} := {{{", ".join(assignments)}}}'
        if self.default_val:
            struct += f' else {self.default_val}'
        return struct


class IDPConstant(IDPFunction):
    """ Class representing an IDP constant.

    IDP constants consist of three components:
        * a symbol_name,
        * its return type;
        * (optionally) its value.

    Attributes:
        symbol_type (str)
        arguments (`list` of str)
        rtype (str): the returntype
        enumeration (`list` of `tuple`)
        arity (int)
        default_val (str)

    """
    def __init__(self, symbol_name: str, returntype: str,
                 value=None):
        super().__init__(symbol_name, [], returntype, {(): value})
        self.symbol_type = 'constant'
        self.value = value

    def to_structure(self):
        """ Convert the Constant into IDP format.
        Args:
            None

        Returns:
            str
        """
        if self.value:
            return f'\t{self.symbol_name} := {self.value}'
        else:
            return ''


class IDPPredicate(IDPFunction):
    """ Class representing an IDP predicate.

    IDP predicates consist of three components:
        * a name;
        * arguments;
        * an enumeration.

    Attributes:
        name (str)
        symbol_type (str)
        enumeration (`list` or `range`)
        arguments (`list` of str)
        arity (int)
    """
    def __init__(self, symbol_name, arguments, enumeration):
        super().__init__(symbol_name, arguments, 'Bool', enumeration)
        self.symbol_type = 'predicate'

    def to_structure(self):
        if len(self.enumeration) == 0:
            return ''
        assignments = []
        if self.arity > 1:
            for key_tuple in self.enumeration:
                key_tuple = [str(x) for x in key_tuple]
                assignments.append(f'({", ".join(key_tuple)})')
        elif self.arity == 1:
            for key in self.enumeration:
                key = str(key)
                assignments.append(f'({key})')
        return f'\t{self.symbol_name} := {{{", ".join(assignments)}}}'


class IDPProposition(IDPPredicate):
    """ Cloass representing an IDP proposition.

    IDP propositions consist of three components:
        * a name;
        * arguments;
        * a value.

    Attributes:
        name (str)
        symbol_type (str)
        enumeration (`list` or `range`)
        arguments (`list` of str)
        arity (int)
        value
    """
    def __init__(self, symbol_name, value):
        super().__init__(symbol_name, [], [value])
        self.symbol_type = 'proposition'
        self.value = value

    def to_structure(self):
        """ Convert the Proposition into its structure format.

        Args:
            None

        Returns:
            str: the Proposition in its structure format
        """
        if self.value:
            return f'\t{self.symbol_name} := true'
        else:
            return ''


class IDPConstraint():
    """ Class representing the IDP constraint.

    Attributes:
        constraint (str): the constrain in FO(.) format.

    """
    def __init__(self, constraint):
        if constraint.endswith('.'):
            self.constraint = constraint
        else:
            self.constraint = f'{constraint}.'

    def to_theory(self):
        """" Returns the constraint

        Return:
            str
        """
        return f'\t{self.constraint}\n'


class IDPDefinition():
    """ Class representing the IDP definition.

    Attributes:
        rules (`list` of str)
    """
    def __init__(self, rules):
        self.rules = []
        for rule in rules:
            if not rule.endswith('.'):
                self.rules.append(rule + '.')
            else:
                self.rules.append(rule)

    def to_theory(self):
        """ Returns the definition in FO(.) format.

        Returns:
            str
        """
        return '\t{\n\t\t' + '\n\t\t'.join(self.rules) + '\n\t}\n'

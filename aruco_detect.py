# Module to handle all aruco-related detections.
# Basically, we have a library of blocks (constant, function, if-then, ...) for
# which we know their aruco ID, and the relative position of their
# children. We build an AST by checking if the `start` block has a block
# attached to it, and then we do the same for that block, etc until we reach
# the end.
#
# Author: Simon Vandevelde (s.vandevelde@kuleuven.be)

import cv2
import math

ARUCO_OFFSET = 29

class Marker():

    def __init__(self, m_id, top_l, top_r, bot_l, bot_r):
        self.id = m_id
        self.top_l = top_l
        self.top_r = top_r
        self.bot_l = bot_l
        self.bot_r = bot_r

        # Used when calculating the positions of other markers.
        self.delta_h = (top_l[0] - bot_l[0], top_l[1] - bot_l[1])
        self.delta_w = (top_l[0] - top_r[0], top_l[1] - top_r[1])
        
        # Calculate the angles of the markers.                  
        self.alpha_h = math.atan2(self.delta_h[1],self.delta_h[0])
        self.alpha_w = math.atan2(self.delta_w[1],self.delta_w[0])
     
        # Overwrite the delta parameters with absolute length.
        self.delta_h = (math.cos(self.alpha_h)*ARUCO_OFFSET, math.sin(self.alpha_h)*ARUCO_OFFSET)
        self.delta_w = (math.cos(self.alpha_w)*ARUCO_OFFSET, math.sin(self.alpha_w)*ARUCO_OFFSET)

        # Used when verifying if a point is inside this marker.
        self.max_x = max(top_l[0], top_r[0], bot_l[0], bot_r[0])
        self.max_y = max(top_l[1], top_r[1], bot_l[1], bot_r[1])
        self.min_x = min(top_l[0], top_r[0], bot_l[0], bot_r[0])
        self.min_y = min(top_l[1], top_r[1], bot_l[1], bot_r[1])

    def draw_self(self, image):
        """ Draw the marker (box, top_l corner, id) on an image """
        cv2.line(image, self.top_l, self.top_r, (0, 255, 0), 2)
        cv2.line(image, self.top_r, self.bot_r, (0, 255, 0), 2)
        cv2.line(image, self.bot_l, self.bot_r, (0, 255, 0), 2)
        cv2.line(image, self.top_l, self.bot_l, (0, 255, 0), 2)

        cv2.circle(image, self.top_l, 2, (255, 0, 0), thickness=5)
        # cv2.putText(image, str(self.id),
        #             (self.top_l[0], self.top_l[1] - 15),
        #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    def slide(self, w, h):
        """ Calculate a slide over of the top-left point over w and h"""
        point = self.top_l

        # Move point h times over marker height.
        point = (int(point[0] + h * self.delta_h[0]),
                 int(point[1] + h * self.delta_h[1]))

        # Move point w times over marker width.
        point = (int(point[0] + w * self.delta_w[0]),
                 int(point[1] + w * self.delta_w[1]))
        return point


class Block():
    """ Parent class for all blocks. """

    def __init__(self, marker):
        self.marker = marker

        # Only some blocks are allowed to be roots, i.e., the start of a rule.
        self.is_root = False

    def draw_marker(self, image):
        self.marker.draw_self(image)

    def contains(self, point):
        """ Check if a (x, y) point is within this block's marker """
        return (self.marker.min_x < point[0] < self.marker.max_x and
                self.marker.min_y < point[1] < self.marker.max_y)

    def search(self, blocks):
        """ Search for sub-blocks in specific places. 
        
        Not applicable for constants."""
        # Implemented in subclasses.
        return

    def draw_code(self, image):
        """ Draw the Block's code on the top-left corner. """
        cv2.putText(image, str(self.code),
                    (self.marker.top_l[0] + 10, self.marker.top_l[1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

    def draw_search(self, image):
        """ Draw where this block looks for others.
        
        Not applicable for constants.
        """
        # Implemented in subclasses.
        return

    def complete(self):
        """ Check if a block can be considered "complete", meaning it can be
        turned into IDP code. """
        return False

class Root(Block):
    """ Represents a root block. """

    def __init__(self, marker):
        super().__init__(marker)

        self.code = ""

        self.child_position = self.marker.slide(1.0, -1.8)
        self.child_block = None

    def __str__(self):
        return f"Root with child {self.child_block}"

    def complete(self):
        """ Root is complete if it has a child, and the child is complete. """
        return (self.child_block and self.child_block.complete())

    def search(self, blocks):
        """ Search for the child block. """
        for block in (blocks['function'] + blocks['predicate']
                      + blocks['junction'] + blocks['implication']
                      + blocks['quantifier'] + blocks['constant']):
            if block.contains(self.child_position):
                self.child_block = block
                self.child_block.search(blocks)
                break

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize child
        cv2.circle(image, self.child_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.child_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate IDP code based on this position. """
        if not(self.complete()):
            return None
        return self.child_block.generate_rule()

class Implication(Block):
    """ Represents an implication block. """

    def __init__(self, marker):
        super().__init__(marker)

        self.code = "Als .. dan .."

        self.antecedent_position = self.marker.slide(1.7, -1.8)
        self.antecedent_block = None

        self.consequent_position = self.marker.slide(1.9, -6.4)
        self.consequent_block = None

    def __str__(self):
        rstr = ""
        if self.antecedent_block and self.antecedent_block:
            rstr = (f"Implication [{self.marker.id}] with antecedent"
                    f" [{self.antecedent_block.marker.id}]"
                    f" and consequent [{self.var_block.marker.id}]")
        return rstr

    def complete(self):
        """ Implication is complete if it contains both antecedent and
        consequent. """
        if not(bool(self.antecedent_block) and bool(self.consequent_block)):
            return False
        return (bool(self.antecedent_block.complete())
                and bool(self.consequent_block.complete()))

    def search(self, blocks):
        """ Search for the inner and outer blocks. """
        self.search_inner(blocks)
        self.search_outer(blocks)

    def search_inner(self, blocks):
        """ A quantifier should contain an antecedent. """

        # Search for the antecedent.
        antecedent_point = self.antecedent_position

        for block in (blocks['function'] + blocks['predicate'] 
                      + blocks['junction']):
            if block.contains(antecedent_point):
                self.antecedent_block = block
                break

        if self.antecedent_block:
            self.antecedent_block.search(blocks)

    def search_outer(self, blocks):
        """ Another block should be attached to a quantifier. """

        # Search for the consequent.
        for block in (blocks['function'] + blocks['predicate'] 
                      + blocks['junction'] + blocks['quantifier']):
            if block.contains(self.consequent_position):
                self.consequent_block = block
                break

        if self.consequent_block:
            self.consequent_block.search(blocks)

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize antecedent
        cv2.circle(image, self.antecedent_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.antecedent_position, (0, 0, 255))

        # Visualize consequent
        cv2.circle(image, self.consequent_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.consequent_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate IDP code based on this position. """
        if not(self.complete()):
            return None
        code = (f"({self.antecedent_block.generate_rule()})"
                f" => ({self.consequent_block.generate_rule()}).")
        return code

class Quantifier(Block):
    """ Represents the "For each ... holds that" block. """

    def __init__(self, marker):
        super().__init__(marker)

        self.code = "Voor alle ... geldt dat"

        self.type_position = self.marker.slide(-1.1, -1.9)
        self.type_block = None

        self.var_position = self.marker.slide(1.2, -1.9)
        self.var_block = None

        self.conn_position = self.marker.slide(-0.4, -4.5)
        self.connected_block = None

        self.is_root = True

    def __str__(self):
        rstr = ""
        if self.type_block and self.var_block:
            rstr = (f"Quantifier [{self.marker.id}] with type"
                    f" [{self.type_block.marker.id}] and variable"
                    f" [{self.var_block.marker.id}]")
        return rstr

    def complete(self):
        """ Quantifier is complete if it contains a type, variable and
        a complete connected block. """
        return (bool(self.type_block) and bool(self.var_block) and
                bool(self.connected_block) and self.connected_block.complete())

    def search(self, blocks):
        """ Search for inner and outer blocks. """
        self.search_inner(blocks)
        self.search_outer(blocks)

    def search_inner(self, blocks):
        """ A quantifier consists of a variable and a type. """

        # Search for the type.
        type_point = self.type_position
        var_point = self.var_position

        for type_block in blocks['type']:
            if type_block.contains(type_point):
                self.type_block = type_block
                break

        # Search for the variable.
        for var_block in blocks['variable']:
            if var_block.contains(var_point):
                self.var_block = var_block
                break

    def search_outer(self, blocks):
        """ Another block should be attached to a quantifier. """

        # Search for the connected block.
        for block in (blocks['function'] + blocks['predicate']
                      + blocks['implication'] + blocks['junction']):
            if block.contains(self.conn_position):
                self.connected_block = block
                break

        if self.connected_block:
            self.connected_block.search(blocks)

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize type
        cv2.circle(image, self.type_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.type_position, (0, 0, 255))

        # Visualize variable
        cv2.circle(image, self.var_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.var_position, (0, 0, 255))

        # Visualize connected
        cv2.circle(image, self.conn_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.conn_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate IDP code based on this position. """

        if not(self.complete()):
            return None
        code = f"!{self.var_block.code} in {self.type_block.code}: "
        code += self.connected_block.generate_rule()
        return code

class Type(Block):
    """ Represents the Type block, e.g. "Country". """
    id_dict = {2: 'component'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

    def complete(self):
        """ Type block is always complete. """
        return True


class Variable(Block):
    """ Represents the variable block, e.g. "x". """
    id_dict = {3: 'o'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

    def complete(self):
        """ Variable block is always complete. """
        return True

class Constant(Block):
    """ Represents a Constant, e.g. "blauw". """
    id_dict = {5: 'blue', 6: 'red', 7: 'green',
               8: 'left_arm', 9: 'right_arm',
               15: 'left_leg', 13: 'right_leg'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

    def complete(self):
        """ Constant block is always complete. """
        return True


class Function(Block):
    """ Represents a function block, such as "Kleur van ... = ..." """
    id_dict = {4: 'Color', 16: 'Color'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

        self.left_position = self.marker.slide(-1.9, -1.5)
        self.left_block = None

        self.right_position = self.marker.slide(0.9, -1.5)
        self.right_block = None

    def __str__(self):
        rstr = ""

    def complete(self):
        """ Function block is complete if both positions are filled. """
        return (bool(self.right_block) and bool(self.left_block))

    def search(self, blocks):
        # Find the left block and right block.
        for block in blocks['constant'] + blocks['variable']:
            if block.contains(self.left_position):
                self.left_block = block

            elif block.contains(self.right_position):
                self.right_block = block

            if self.left_block and self.right_block:
                break

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize left
        cv2.circle(image, self.left_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.left_position, (0, 0, 255))

        # Visualize right
        cv2.circle(image, self.right_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.right_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate this blocks IDP code. """
        return f"{self.code}({self.left_block.code}) = {self.right_block.code}"

class Junction(Block):
    """ Represents a con- or disjunction block """
    id_dict = {14: '|', 10: '&'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

        self.left_position = self.marker.slide(7.3, -1.7)
        self.left_block = None

        self.right_position = self.marker.slide(1.3, -1.5)
        self.right_block = None

    def __str__(self):
        rstr = ""
        return ""

    def complete(self):
        """ Function block is complete if both positions are filled. """
        return (bool(self.right_block) and bool(self.left_block) and self.right_block.complete() and self.left_block.complete())

    def search(self, blocks):
        # Find the left block and right block.
        for block in (blocks['function'] + blocks['predicate']
                      + blocks['quantifier'] + blocks['implication']):
            if block.contains(self.left_position):
                self.left_block = block
                self.left_block.search(blocks)

            elif block.contains(self.right_position):
                self.right_block = block
                self.right_block.search(blocks)

            if self.left_block and self.right_block:
                break

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize left
        cv2.circle(image, self.left_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.left_position, (0, 0, 255))

        # Visualize right
        cv2.circle(image, self.right_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.right_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate this blocks IDP code. """
        if not self.complete():
            return None
        return f"({self.left_block.generate_rule()}) {self.code} ({self.right_block.generate_rule()})"


class Predicate(Block):
    """ Represents a predicate block, such as "is an arm" """
    id_dict = {11: 'IsArm', 16: 'IsLeg'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

        self.inner_position = self.marker.slide(-2.0, -1.4)
        self.inner_block = None

    def __str__(self):
        rstr = ""

    def complete(self):
        """ Predicate block is complete if inner position is filled. """
        return bool(self.inner_block)

    def search(self, blocks):
        # Find the inner block.
        for block in blocks['constant'] + blocks['variable']:
            if block.contains(self.inner_position):
                self.inner_block = block
                break

    def draw_search(self, image):
        """ Visualize the search for blocks. """

        # Visualize inner
        cv2.circle(image, self.inner_position, 2, (0, 0, 255), thickness=5)
        cv2.line(image, self.marker.top_l, self.inner_position, (0, 0, 255))

    def generate_rule(self):
        """ Generate this blocks IDP code. """
        return f"{self.code}({self.inner_block.code})"


class Proposition(Block):
    """ Represents a proposition, such as "has helmet" """
    id_dict = {12: 'helmet'}

    def __init__(self, marker):
        super().__init__(marker)

        self.code = self.id_dict[marker.id]

    def __str__(self):
        return f"{self.code}"

    def generate_rule(self):
        """ Generate this blocks IDP code. """
        return f"{self.code}()"

    def complete(self):
        return True


def interpret_image(image):
    """ Returns the IDP formula in the image. """
    aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_1000)
    aruco_params = cv2.aruco.DetectorParameters_create()
    (corners, ids, rejected) = cv2.aruco.detectMarkers(image, aruco_dict,
                                                       parameters=aruco_params)
    if not corners:
        cv2.imshow('detected', cv2.resize(image, (340, 420)))
        cv2.waitKey(1)
        return


    blocks = {'quantifier': [], 'type': [], 'variable': [],
              'predicate': [], 'function': [], 'constant': [],
              'implication': [], 'junction': [], 'root': []}

    for (m_corner, m_id) in zip(corners, ids):
        (top_l, top_r, bot_r, bot_l) = m_corner.reshape((4, 2))
        top_l = (int(top_l[0]), int(top_l[1]))
        top_r = (int(top_r[0]), int(top_r[1]))
        bot_l = (int(bot_l[0]), int(bot_l[1]))
        bot_r = (int(bot_r[0]), int(bot_r[1]))

        marker = Marker(m_id[0], top_l, top_r, bot_l, bot_r)

        block = None
        if m_id == 0:
            block = Implication(marker)
            blocks['implication'].append(block)
        elif m_id == 17:
            block = Root(marker)
            blocks['root'].append(block)
        elif m_id == 1:
            block = Quantifier(marker)
            blocks['quantifier'].append(block)
        elif m_id == 2:
            block = Type(marker)
            blocks['type'].append(block)
        elif m_id == 3:
            block = Variable(marker)
            blocks['variable'].append(block)
        elif m_id == 4:
            block = Function(marker)
            blocks['function'].append(block)
        elif m_id in [5, 6, 7, 8, 9, 13, 15]:
            block = Constant(marker)
            blocks['constant'].append(block)
        elif m_id in [11, 16]:
            block = Predicate(marker)
            blocks['predicate'].append(block)
        elif m_id == 12:
            block = Proposition(marker)
            blocks['predicate'].append(block)
        elif m_id in [10, 14]:
            block = Junction(marker)
            blocks['junction'].append(block)
        else:
            block = Block(marker)
            block.draw_marker(image)

    rules = []
    for sub_blocks in blocks.values():
        for block in sub_blocks:
            # Draw the block and draw its search.
            block.draw_search(image)
            block.draw_marker(image)
            block.draw_code(image)

    # If a root is present, generate the rule.
    if len(blocks['root']) > 0:
        root = blocks['root'][0]
        root.search(blocks)
        if root.complete():
            rules.append(root.generate_rule())

    # cv2.imwrite("aruco-detected.png", image)

    cv2.imshow('detected', cv2.resize(image, (340, 420)))
    cv2.waitKey(1)
    return [x for x in rules if x]


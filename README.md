# FOLL-E

FOLL-E (First Order Logic Learning Environment) aims to teach logic to children, as a way to sharpen their real-life reasoning skills.
This repo contains the code and designs of the game, designed to be run off a Raspberry Pi.
The philosophy of our tool is as follows:

* It is syntax-less, due to the puzzle-based nature
* There is a minimal amount of text 
* The tool is highly interactive: it gives immediate feedback 
* This feedback should be visual
* Programming should be tangible, hence the physical blocks.

The design of the puzzle pieces is inspired from systems such as Scratch and Alice, but adapted specifically for First Order Logic.
A brief demo of the tool is [available online](https://www.youtube.com/watch?v=7UkZ0Dq3uPU).

## Installation

Installing FOLL-E is very easy.
First, clone this repo, and then install the requirements.

```
git clone https://gitlab.com/Vadevesi/foll-e
cd foll-e
pip3 install -r requirements.txt
```

The detection of the various blocks is done by OpenCV. Sadly, the default Python opencv package does not contain the required AruCo marker library, so you have to [compile OpenCV (version 3.4.18.65) from scratch](https://docs.opencv.org/4.x/df/d65/tutorial_table_of_content_introduction.html).
The AruCo marker library is in OpenCV_contrib, don't forget to build this one!
Additionally, make sure to build the Python bindings.

### Enabling autostart

It is possible to have FOLL-E start automatically after the Pi is done booting.
This is also fairly straightforward: there's a bash script included in this repo that can be run using the Pi's `autostart`.
Note: it assumes that FOLL-E is located in `/home/pi/foll-e`.

First, make sure the script can be executed:
```
chmod +x folle.sh
```

Then, add the following to `/etc/xdg/lxsession/LXDE-pi/autostart`, right before the `xscreensaver` line.

```
@/home/pi/foll-e/folle.sh
```

That's it!

## Usage

```
python3 draw_robot.py
```

## Code

The code consists of two modules: `aruco_detect.py` and `draw_robot.py`.

`aruco_detect.py` contains all the code for the detection of the aruco markers.
In a nutshell, it maps each aruco marker on a logical puzzle piece using a dictionary.
For each piece, it knows whether it should have one or more *child pieces* that need to be found, such as the *IF .. THEN*'s antecedent and conclusion.
Starting at the `start` block, the completeness of the sentence is checked by recursively moving through it.

`draw_robot.py` draws the graphical environment, and performs the communication with the IDP-Z3 system.
The GUI is created using [pygame](https://www.pygame.org/news).
The six *minibots* on the sides of the window each represent a possible world, with the left ones being sat and the right ones being unsat.
The *propagation bot* visualizes the player's current rule by propagating it and drawing the result.
These bots have been illustrated by [Kaat De Bock](kaatdebock.be).

The communication with the IDP-Z3 system is handled by IDP-Z3Py.
Each time a complete sentence is puzzled together, the system performs 7 calls to the IDP-Z3 system: 6 `sat_checks` to see if which minibots are correctly (im)possible, and one `propagation` to visualise the effect of the sentence.

## DIY FOLL-E box.

`foll-e-resources.svg` contains all the images required to build a FOLL-E setup + block box using a laser-cutter. 
We will be sharing a guide on how to build your own FOLL-E boxes soon.
In the meantime, the short explanation is as follows:

* The layer `Box sides` contains the SVGs necessary to create the box using a laser cutter
* The layers `Block Box L1-L5` contain all the blocks that you will need to cut out. This was designed in such a way that you will end up with a small box at the end, in which you can fit all the pieces.
* The layer `aruco` contains all the required aruco markers. You can either cut these manually out of paper, or you can use the `aruco cut` sheet to order sticker sheets.

After building the box, cutting out the blocks, and gluing the correct aruco markers, you can place the raspberry pi (with raspicam) in the box.
For the top, translucent pane of the box, you can visit the local DIY store and have them cut it out of plastic or other similar material.

For ideal results, you'll want to have some sort of light source in the box itself.
The easiest solution is to use the flashlight of your smartphone, but you can also set up a LED strip.


## Cite us

If you have used this code or you want to cite us, you can do so as follows:

Vandevelde, S., Vennekens, J. (2022). FOLL-E: Teaching First-Order Logic to Children. In: Proceedings of the 37th AAAI Conference on Artificial Intelligence. AAAI Conference on Artificial Intelligence. Presented at the Thirteenth AAAI Symposium on Educational Advances in Artificial Intelligence, Washington, D.C., USA, 11 Feb 2023-12 Feb 2023. 

```
@article{Vandevelde_Vennekens_2024,
    title={FOLL-E: Teaching First Order Logic to Children},
    volume={37},
    url={https://ojs.aaai.org/index.php/AAAI/article/view/26884},
    DOI={10.1609/aaai.v37i13.26884},
    abstractNote={First-order logic (FO) is an important foundation of many domains, including computer science and artificial intelligence. In recent efforts to teach basic CS and AI concepts to children, FO has so far remained absent. In this paper, we examine whether it is possible to design a learning environment that both motivates and enables children to learn the basics of FO. The key components of the learning environment are a syntax-free blocks-based notation for FO, graphics-based puzzles to solve, and a tactile environment which uses computer vision to allow the children to work with wooden blocks. The resulting FOLL-E system is intended to sharpen childrens’ reasoning skills, encourage critical thinking and make them aware of the ambiguities of natural language. During preliminary testing with children, they reported that they found the notation intuitive and inviting, and that they enjoyed interacting with the application.},
    number={13},
    journal={Proceedings of the AAAI Conference on Artificial Intelligence},
    author={Vandevelde, Simon and Vennekens, Joost},
    year={2024},
    month={Jul.},
    pages={15869-15876}
}
```



